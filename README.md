# `pg-slowlog-extractor`

This program is a tiny utility for converting PostgreSQL slow logs into easy-to-ingest new-line-delimited JSON (NDJSON).

Postgres slowlogs can be tricky to parse as the log lines usually exceed a single line.

## How does it work?

Either pipe PostgreSQL slowlogs in via stdin, or pass a file argument.

```console
$ # build the binary
$ cargo build
$ # pass logs in via stdin (useful for streaming)
$ cat postgresql.log.2023-09-14-04|pgslowlog
{ ... }
$ pgslowlog postgresql.log.2023-09-14-04
{ ... }
```

## Installation

The latest package can be downloaded and installed from the Releases page:

https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/library/pg-slowlog-extractor/-/releases

## Alternatively, if you have `curl` installed, you can install the latest version using the following:

```console
release_url=$(curl --silent https://gitlab.com/api/v4/projects/gitlab-com%2fgl-infra%2fgitlab-dedicated%2flibrary%2fpg-slowlog-extractor/releases |
  jq --arg os "$(uname -s)" --arg machine "$(uname -m)" -r '[.[0]|.assets.links[]|select(.name | contains($os|ascii_downcase))|select(.name | contains($machine))]|first|.url' )
curl "$release_url" -o pg-slowlog-extractor.tgz
tar -xvzf pg-slowlog-extractor.tgz
# Option, install to /usr/local/bin
sudo install pgslowlog /usr/local/bin
```

## Output

Each line output from `pgslowlog` will have a shape similar to this example:

```json
{
  "time": "2023-09-14T04:00:37Z",
  "duration_ms": 2224.436,
  "query": "/*application:sidekiq,correlation_id:4253b3d52cc3f5c5857ce81dac522125,jid:4f962a5fa4425fde608b2283,endpoint_id:Analytics::CycleAnalytics::ConsistencyWorker,db_config_name:main*/ SELECT ...",
  "fingerprint": "bb084cbd13e42905",
  "marginalia": {
    "correlation_id": "4253b3d52cc3f5c5857ce81dac522125",
    "db_config_name": "main",
    "application": "sidekiq",
    "jid": "4f962a5fa4425fde608b2283",
    "endpoint_id": "Analytics::CycleAnalytics::ConsistencyWorker"
  }
}
```

This can then be used for further analysis using other tools,
such as [Angle-grinder](https://github.com/rcoh/angle-grinder) or [GNU Datamash](https://www.gnu.org/software/datamash/).

## Snippets for working with Postgres logs

See [`snippets.md`](./docs/snippets.md) for some snippets and example usage of useful queries.
