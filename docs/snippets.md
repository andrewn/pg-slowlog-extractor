# Snippets for using `pg-slowlog-extractor`

## Using with **Angle-grinder**

[Angle-grinder ](https://github.com/rcoh/angle-grinder) allows you to parse, aggregate, sum, average, min/max, percentile, and sort your data.
You can see it, live-updating, in your terminal.
Angle grinder is designed for when, for whatever reason, you don't have your data in graphite/honeycomb/kibana/sumologic/splunk/etc,
but still want to be able to do sophisticated analytics.

To install Angle-grinder on Macos with Homebrew, you can use:

```shell
brew install angle-grinder
```

Alternative installation methods are available in the [Angle-grinder README.MD file](https://github.com/rcoh/angle-grinder?tab=readme-ov-file#installation).

### Analyse slowlog durations by `endpoint_id`

This query will give you an idea of which endpoints are resulting in slow queries

```console
$ pgslowlog postgresql.log | agrind '* | json | where duration_ms > 0 | sum(duration_ms) as total_duration_ms by marginalia.endpoint_id'
marginalia.endpoint_id                  total_duration_ms
---------------------------------------------------------
POST /api/:version/jobs/request         6291457.85
PostReceive                             3882249.22
PipelineProcessWorker                   3292124.24
ProjectDestroyWorker                    3230933.89
StoreSecurityReportsWorker              2537470.70
None                                    1861604.03
Ci::InitialPipelineProcessWorker        1854660.48
GraphqlController#execute               1762984.88
Ci::ArchiveTraceWorker                  1755330.96
Ci::BuildFinishedWorker                 1469804.96
PUT /api/:version/jobs/:id              1376215.26
RepositoryUpdateMirrorWorker            1062436.36
Security::StoreScansWorker              998315.56
UpdateMergeRequestsWorker               978584.84
POST /api/:version/projects/:id/pipeline963156.85
```

### Analyse slowlog durations by `fingerprint`

`pgslowlog` will generate a SQL query fingerprint for each SQL query in the log file.

This allows structurally similar (but not identical) SQL queries to be grouped together for analysis.

```console
$ pgslowlog postgresql.log | agrind '* | json | where duration_ms > 0 | sum(duration_ms) as total_duration_ms by fingerprint'
fingerprint             total_duration_ms
-------------------------------------------------
295ba7daf0143738        3718696.64
a53cde8092d4d79f        2027563.90
17c31301489dde15        2011656.31
a078931445d4aeaa        1663403.18
0cfed128cd67fc8c        1433650.45
5fd831f9cba2a1ea        1314178.75
cc58327ca0dda331        1156150.70
fc3ee2b273c08b45        1084718.11
5e80e4b5dfa193d1        1029544.15
618cf2ff939b19a2        901672.02
2bd6b40ce480dd4a        755628.85
d190c3e5bc1736fd        707670.45
bef9c40c9811e576        680018.58
86349224f16f68d7        633863.29
adde68562a8fa535        613652.56
187db0bbee939762        590036.16
8535ec33d8f3e55a        588058.19
1afa1fc92ee1ae42        580977.29
27a0773bed17fec1        565769.03
ec76793c180b9085        555778.26
```

### List queries given a fingerprint

Once you have a fingerprint you would like to investigate, use the following query to list some examples of it.

```console
$ # Search for top logs for fingerprint 86349224f16f68d7
$ pgslowlog postgresql.log | agrind '*|json|where fingerprint == "86349224f16f68d7" | limit 10'  --output logfmt
duration_ms=11106.43 fingerprint=86349224f16f68d7 marginalia={application:sidekiq, correlation_id:a1ab52a6fdb0abeec8000e898fbc3014, db_config_name:main, endpoint_id:ContainerExpirationPolicies::CleanupContainerRepositoryWorker, jid:c7c82e792e695223cbdcea09} query=/*application:sidekiq,correlation_id:a1ab52a6fdb0abeec8000e898fbc3014,jid:c7c82e792e695223cbdcea09,endpoint_id:ContainerExpirationPolicies::CleanupContainerRepositoryWorker,db_config_name:main*/ UPDATE "container_repositories" SET "updated_at" = '2023-09-19 07:00:47.059944', "expiration_policy_started_at" = '2023-09-19 07:00:47.027571', "expiration_policy_cleanup_status" = 3 WHERE "container_repositories"."id" = 2545 time=2023-09-19T07:00:58Z
duration_ms=26627.69 fingerprint=86349224f16f68d7 marginalia={application:sidekiq, correlation_id:9ac5bee2fbf9cdda4c3e2a9e414a69f2, db_config_name:main, endpoint_id:ContainerExpirationPolicies::CleanupContainerRepositoryWorker, jid:ce3d922e04b12d8999d539f2} query=/*application:sidekiq,correlation_id:9ac5bee2fbf9cdda4c3e2a9e414a69f2,jid:ce3d922e04b12d8999d539f2,endpoint_id:ContainerExpirationPolicies::CleanupContainerRepositoryWorker,db_config_name:main*/ UPDATE "container_repositories" SET "updated_at" = '2023-09-19 07:03:16.613805', "expiration_policy_started_at" = '2023-09-19 07:03:16.597834', "expiration_policy_cleanup_status" = 3 WHERE "container_repositories"."id" = 693 time=2023-09-19T07:03:43Z
```
