use std::env;
use std::io::{self, BufReader, BufRead};
use std::fs::File;
use log::debug;

mod stream;
mod slowlog;


fn main() -> io::Result<()> {
    env_logger::init();

    let args: Vec<String> = env::args().skip(1).collect();
    let buf: Box<dyn BufRead>;

    if args.is_empty() {
        debug!("Reading from stdin");
        buf = Box::new(io::stdin().lock());

    } else {
        let arg1 = args.first().unwrap();
        debug!("Reading from file: {}", arg1);

        let f = File::open(arg1)?;

        buf = Box::new(BufReader::new(f));
    }

    let parser = stream::Parser::new(buf);
    let sm = slowlog::SlowLogMatcher::new();

    let mut iter = parser
        .filter_map(|f| sm.parse_line(&f));

    while let Some(y) = iter.next() {
        let j = serde_json::to_string(&y)?;

        println!("{}", j);
    }

    Ok(())
}
