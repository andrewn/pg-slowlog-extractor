use std::collections::HashMap;

use regex::Regex;
use chrono::{DateTime, FixedOffset};
use serde::{Deserialize, Serialize};

#[derive(Debug)]
#[derive(Serialize, Deserialize)]
pub struct SlowLog {
    pub time: DateTime<FixedOffset>,
    pub duration_ms: f64,
    pub query: String,
    pub fingerprint: String,
    pub marginalia: HashMap<String, String>
}

pub struct SlowLogMatcher {
    full_line_re: Regex,
}

impl SlowLogMatcher {
    pub fn new() -> Self {
        Self {
            full_line_re: Regex::new(r"^(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2})\s+([\w\d\-]+):[^:]*:[^:]*:[^:]*:\w+:\s+duration:\s*(\d+(?:\.\d+))\s*ms\s+execute[^:]+:\s*(.*)").unwrap(),
        }
    }

    pub fn parse_line(&self, current_log: &String) -> Option<SlowLog> {
        if current_log.len() == 0 {
            return None
        }

        let caps = match self.full_line_re.captures(current_log.as_str()) {
            Some(x) => x,
            None => return None
        };

        let date = caps.get(1).unwrap().as_str();
        let mut tz = caps.get(2).unwrap().as_str();
        let duration= caps.get(3).unwrap().as_str();
        let query= caps.get(4).unwrap().as_str();

        // chrono doesn't like Postgres using UTC. Convert to +0000
        if tz == "UTC" {
            tz = "+0000";
        }

        let full_date = format!("{date} {tz}");
        let log_date = match DateTime::parse_from_str(full_date.as_str(), "%F %T %z") {
            Ok(s) => s,
            Err(_err) => return None
        };

        let duration_ms: f64 = duration.parse().unwrap_or_default();
        let marginalia = self.extract_marginalia(query.to_string());

        let r = SlowLog{
            time: log_date,
            duration_ms,
            query: query.to_string(),
            fingerprint: self.fingerprint(query.to_string()).unwrap_or_default(),
            marginalia: marginalia,
        };

        return Some(r)
    }

    fn fingerprint(&self, query: String) -> Option<String> {
        match pg_query::fingerprint(&query) {
            Ok(r) => Some(r.hex),
            Err(_err) => None,
        }
    }

    /* application:sidekiq,correlation_id:01HA900PRSF9GK8WCGABSD5FCW,jid:e3b45a3e9a4250c0edaa778c,endpoint_id:Ci::PipelineSuccessUnlockArtifactsWorker,db_config_name:ci */
    fn extract_marginalia(&self, query: String) -> HashMap<String, String> {
        let mut m: HashMap<String, String> = HashMap::new();

        let start = match query.splitn(2, "/*").skip(1).next() {
            Some(s) => s,
            None => return m
        };

        let marginalia = match start.split("*/").next() {
            Some(s) => s,
            None => return m
        };

        let pairs = marginalia.split(",");

        for p in pairs {
            if let Some((key, value)) = p.split_once(':') {
                let trimmed_key = key.trim().to_string();
                let trimmed_value = value.trim().to_string();

                m.insert(trimmed_key, trimmed_value);
            }
        }

        m
    }

}
