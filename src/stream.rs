use std::io::{BufRead, Lines};
use regex::Regex;

pub struct Parser {
    date_re: Regex,
    lines: Lines<Box<dyn BufRead>>,
    current_log: String,
}

impl Parser {
    pub fn new(buf: Box<dyn BufRead>) -> Self {
        Self {
            date_re: Regex::new(r"^\d{4}-\d{2}-\d{2} ").unwrap(),
            lines: buf.lines(),
            current_log: String::new()
        }
    }
}

impl Iterator for Parser {
    // We can refer to this type using Self::Item
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        while let Some(line) = self.lines.next() {
            let last_input = line.unwrap(); // This will panic on non unicode text

            if !last_input.is_empty() && self.date_re.is_match(last_input.as_str()) {
                let prev_log = self.current_log.clone();
                self.current_log.clone_from(&last_input);

                return Some(prev_log);
            }

            // add a new line once user_input starts storing user input
            if self.current_log.is_empty() {
                self.current_log.push_str("\n");
            }

            // store user input
            self.current_log.push_str(&last_input);
        }

        if self.current_log.len() > 0 {
            let prev_log = self.current_log.clone();
            self.current_log.clear();

            return Some(prev_log);
        }

        None
    }
}
